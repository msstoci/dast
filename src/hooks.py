from custom_hooks import CustomHooks

# Implementation for ZAP scan hooks. For more info, see
# https://github.com/zaproxy/zaproxy/blob/develop/docker/docs/scan-hooks.md

hooks = CustomHooks()
zap_access_target = hooks.zap_access_target
cli_opts = hooks.cli_opts
zap_pre_shutdown = hooks.zap_pre_shutdown
