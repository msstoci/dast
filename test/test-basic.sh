#!/bin/sh

check_result() {
  set +e

  # Compare results
  jq '.site' gl-dast-report.json | diff -u test/expect/site-basic.json -

  if [ $? = 1 ] ; then
    echo "Analyze results differ from expectations"
    exit 1
  fi

  jq '.spider.result.urlsInScope | .[].url' gl-dast-report.json | sort | diff -u test/expect/basic-urls.txt -

  if [ $? = 1 ] ; then
    echo "Spidered URLs differ from expectations"
    exit 1
  fi

  rm -f gl-dast-report.json

  set -e
}

# Cleanup
trap "set +e; (docker rm -f nginx ; docker network rm test) > /dev/null; docker rm -f opt-param-test" EXIT
rm -f gl-dast-report.json

# Install jq if absent
which jq > /dev/null || apk add jq

BUILT_IMAGE=${BUILT_IMAGE:-dast}

# Run ZAP on a small vulnerable web form
set -ex

# build the image locally if not in the pipeline
if [[ "$BUILT_IMAGE" = "dast" ]]; then
  docker build --pull --quiet -t ${BUILT_IMAGE} .
fi

docker network create test
docker run --name nginx -v ${PWD}/test/web:/usr/share/nginx/html:ro --network test -d nginx


echo "Test missing parameter"
set +e
docker run --rm -v ${PWD}:/output --network test ${BUILT_IMAGE} /analyze
if [ $? -ne 1 ]; then
  echo "DAST_WEBSITE is empty, the script should have failed with exit code 1"
  exit 1
fi
set -e


echo "Test with -t param"
docker run --rm -v ${PWD}:/output --network test ${BUILT_IMAGE} /analyze -t http://nginx

check_result


echo "Test with environment variable"
docker run --rm -v ${PWD}:/output --network test -e DAST_WEBSITE=http://nginx ${BUILT_IMAGE} /analyze

check_result


echo "Test passing optional params to ZAP"
docker run --name 'opt-param-test' -v ${PWD}:/output --network test -e DAST_WEBSITE=http://nginx ${BUILT_IMAGE} /analyze -t 'http://nginx' -j -z very_optional_param
set +e
docker logs 'opt-param-test' 2>&1 | grep 'Script params:' | grep "('-j', ''), ('-z', 'very_optional_param')"


if [ $? = 1 ] ; then
  echo 'Expected optional params "-j -z very_optional param" passed to ZAP, but none were passed.'
  exit 1
fi
set -e
