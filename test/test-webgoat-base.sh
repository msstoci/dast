#!/usr/bin/env sh

# Check that the spider found the expected URLs
check_urls() {
    set +e

    jq '.spider.result.urlsInScope | .[].url' gl-dast-report.json | sort | diff -u test/expect/webgoat-urls.txt -

    if [ $? = 1 ] ; then
        echo "spidered urls differ from expectations"
        exit 1
    fi

    set -e
}

# Cleanup
trap "set +e; (docker rm -f goat ; docker network rm test ; rm -r test/.webgoat-8.0.0.M21) > /dev/null" EXIT
rm -f gl-dast-report.json

BUILT_IMAGE=${BUILT_IMAGE:-dast}

set -ex

# build the image locally if not in the pipeline
if [[ "$BUILT_IMAGE" = "dast" ]]; then
  docker build --pull -t ${BUILT_IMAGE} .
fi

# Start webgoat
WEBGOAT_IMAGE='registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e'
WEBGOAT_USER='someone'
WEBGOAT_PASS='P@ssw0rd'
docker network create test
tar -xzvf test/webgoat-data.tar.gz -C ./test
docker run --rm -v ${PWD}/test/.webgoat-8.0.0.M21:/home/webgoat/.webgoat-8.0.0.M21 --name goat --network test -d ${WEBGOAT_IMAGE}

# Run DAST against webgoat
docker run -e DAST_FULL_SCAN_ENABLED --rm -v ${PWD}:/output --network test ${BUILT_IMAGE} /analyze -t http://goat:8080/WebGoat/attack \
  --auth-url http://goat:8080/WebGoat/login \
  --auth-username $WEBGOAT_USER \
  --auth-password $WEBGOAT_PASS \
  --auth-username-field "exampleInputEmail1" \
  --auth-password-field "exampleInputPassword1" \
  --auth-exclude-urls 'http://goat:8080/WebGoat/logout,http://goat:8080/WebGoat/login?logout'
